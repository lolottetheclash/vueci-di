import { fetchNominatimResult } from '../../src/helpers/fetchNominatimResult'
import axios from 'axios'

const originalGet = axios.get

describe('fetchNominatimResult', () => {
  it('fetch the right URL', async () => {
    axios.get = jest.fn((url) => {
      return Promise.resolve({ data: 'pouetpouet' })
    })
    fetchNominatimResult('pouic')
  })
})
