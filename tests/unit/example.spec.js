import { shallowMount } from '@vue/test-utils'
import Home from '@/views/Home.vue'
import { getQuery } from '../../src/helpers/getQuery'
import { fetchNominatimResult } from '../../src/helpers/fetchNominatimResult'

describe('Home.vue', () => {
  it('render without crash', () => {
    shallowMount(Home)
  })
  it('match shallow snapshot', () => {
    const wrapper = shallowMount(Home)
    expect(wrapper).toMatchSnapshot()
  })
  // it('match mount snapshot', () => {
  //   const wrapper = mount(Home)
  //   expect(wrapper).toMatchSnapshot()
  // })
  it('return the right query string without params', () => {
    expect.assertions(2)
    expect(getQuery()).toEqual(`data=[out:xml];(way[%22leisure%22=%22park%22](43.54506428956427,1.3108062744140625,43.663525432098275,1.571388244628906););out%20body;%3E;out%20skel%20qt;`)
    expect(getQuery()).toBe(`data=[out:xml];(way[%22leisure%22=%22park%22](43.54506428956427,1.3108062744140625,43.663525432098275,1.571388244628906););out%20body;%3E;out%20skel%20qt;`)
  })

  it('return the right query string', () => {
    expect(getQuery(undefined)).toEqual(`data=[out:xml];(way[%22leisure%22=%22park%22](43.54506428956427,1.3108062744140625,43.663525432098275,1.571388244628906););out%20body;%3E;out%20skel%20qt;`)
  })

  it('must raise an error because wrong parameter', () => {
    expect(() => getQuery(null)).toThrow()
  })
})
