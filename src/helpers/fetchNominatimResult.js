import axios from 'axios'

export async function fetchNominatimResult (search = 'toulouse') {
  const response = await axios.get(
    'http://nominatim.openstreetmap.org/search/' +
      search +
      '?format=json'
  )
  return response.data
}
